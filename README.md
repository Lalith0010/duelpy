# duelpy

This is a python package for solving with Preference Based Multi Armed Bandit problems, also known as dueling bandits. Refer to [this paper](https://arxiv.org/abs/1807.11398) for an overview of the field.
